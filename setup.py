from setuptools import find_packages, setup

setup(
    name='atmocal',
    packages=find_packages('atmocal'),
    version='0.0.0',
    description='atmosphere calculations',
    author='Harm Schoorlemmer',
    license='GNU')
