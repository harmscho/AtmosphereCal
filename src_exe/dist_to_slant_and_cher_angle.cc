#include "Atmosphere.h"
#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;

int main(int argc, char** argv) {
  
  if (argc != 4) {
    cout << "Usage ./dist_to_slant z_gr zen X" << endl;
    return 0;
  }
  
  Atmosphere atm(eLinsley);
  
  
  double d2r = M_PI / 180;//degree to rad
  double zGround = atof(argv[1]); // 50 m
  double zenithAngle = atof(argv[2]) * d2r;//zenith angle
  double slantDepth = atof(argv[3]); // g / cm^2
  
//  cout << "\n======Example=====" << endl;
//  cout << "Using a Linsley atmosphere,\naltitude of ground = 50 m a. s. l,\nZenith angle of 70 deg.,\nSlant depth of Xmax is 650 g/cm^2 " << endl;
//  cout << "\n------Results-----" << endl;

  double dXmax = atm.GetDistanceToSlantDepth(zenithAngle,slantDepth,zGround);
  

  double altitudeXmax = atm.VerticalHeight(dXmax * sin(zenithAngle), dXmax * cos(zenithAngle),zGround);
//  cout  << "Xmax is at an altitude of " <<altitudeXmax/1e3 << "[km] above sea level." << endl;

  double eta = atm.RefractivityAtHeight(altitudeXmax);
  double psi_C = acos(1./(1+ eta));
//
//  cout << "The refractivy at Xmax is " << eta << "," <<endl;
//  cout << "resulting in a Cherenkov angle of " << psi_C/d2r << " [deg]\n\n" <<  endl;
  cout << dXmax << " m\t" <<  psi_C/d2r << " [deg]" <<endl;
  return 0;
}
