# AtmosphereCal
Code to perform density related calculations in the atmosphere.

Both C++ and python implementation.

for C++:
```
make
```

for python
```
pip install -e .
```
