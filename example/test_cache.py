#%%
import numpy as np
import matplotlib.pyplot as plt
from atmocal import *
atm = AtmoCal()
zenith = np.deg2rad(np.linspace(0,85,86))#zenith in radians
# zenith
z_gr = 1400#m
#%%

Xs = np.linspace(600,1000,9) #g/cm2
for X in Xs:
    d = [atm.distance_to_slant_depth(z,X,z_gr)/1e3 for z in zenith]
    plt.plot(np.rad2deg(zenith),d,label="X = %d g cm$^{-2}$"%(X))
plt.yscale('log')
plt.ylim(0.1,500)
plt.xlabel(r'zenith angle ($^\circ$)')
plt.ylabel(r'distance to slant depth (km)')
plt.grid()
plt.legend()
#%%
h = np.linspace(0,300e3,1000)
x = [ atm.vertical_depth_at_height(h_) for h_ in h]
plt.plot(h[:400]/1e3,x[:400])
plt.yscale('log')
# %%
h = np.linspace(0,300e3,1000)
x = [ atm.density_at_height(h_) for h_ in h]
plt.plot(h[:400]/1e3,x[:400])
plt.yscale('log')
# %%
n = [atm.refractivity_at_height(h_) for h_ in h] 
plt.plot(h[:400]/1e3,n[:400])
plt.yscale('log')
# %%
atm2 = AtmoCal(GDASFile='./GDASXXXXX.txt')
print(atm2.gdas_atmlay)
print(atm2.gdas_A)
print(atm2.gdas_B)
print(atm2.gdas_C)
plt.plot(atm2.gdas_h,atm2.gdas_n)
print(atm2.gdas_n[-1])

# %%
h = np.linspace(0,300e3,1000)
x = [ atm.vertical_depth_at_height(h_) for h_ in h]
plt.plot(h[:100]/1e3,x[:100])
h = np.linspace(0,300e3,1000)
x = [ atm2.vertical_depth_at_height(h_) for h_ in h]
plt.plot(h[:100]/1e3,x[:100])
# plt.yscale('log')

# %%
h = np.linspace(0,300e3,1000)
x = [ atm.refractivity_at_height(h_) for h_ in h]
plt.plot(h[:100]/1e3,x[:100])
h = np.linspace(0,300e3,1000)
x = [ atm2.refractivity_at_height(h_) for h_ in h]
plt.plot(h[:100]/1e3,x[:100])
plt.plot(atm2.gdas_h/1e3,atm2.gdas_n-1)
# %%
