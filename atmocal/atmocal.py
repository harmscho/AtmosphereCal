import numpy as np
from functools import lru_cache 

class AtmoCal:
    """
        Atmospheric Calculation class
        Calculations of atmospheric properties
        in curved geomtry:
        - Density
        - Slant depth (grammage)
        - Refractive Index
        - Light travel time
    """
    def __init__(self,density_name="USStandard",refr_name="zhaires",GDASFile=""):
        self.rEarth = 6371e3
        self.rEarthSquared = self.rEarth**2
        self.dEarth = 2*self.rEarth
        self.dEarthSquared = self.dEarth**2
        self.speed_of_light = 299792458.
        self.vdepth = []#cached values
        self.refrac = []#cached values
        if len(GDASFile) > 0:
            self.load_gdas(GDASFile)
        else:
            self.density_name = density_name
            self.refr_name = refr_name

    def load_gdas(self,GDASFile):
        fgdas = open(GDASFile)
        lines = fgdas.readlines()
        self.vdepth = []
        self.refrac = []
        #check
        if lines[0][0] != '#':
            print('expected # on reading line 0, instead found: ',lines[0][0])
        elif lines[5][0] != '#':
            print('expected # on reading line 5, instead found: ',lines[5][0])
        else:
            l = lines[1].split()
            self.gdas_atmlay = np.array([float(l_) for l_ in l])/100 #assume it is given in cm
            l = lines[2].split()
            self.gdas_A = np.array([float(l_) for l_ in l])
            l = lines[3].split()
            self.gdas_B = np.array([float(l_) for l_ in l])
            l = lines[4].split()
            self.gdas_C = np.array([float(l_) for l_ in l])/100 #assume it is given in cm
            self.gdas_h =np.array([float(l.split()[0]) for l in lines[6:]])
            self.gdas_n =np.array([float(l.split()[1]) for l in lines[6:]])
            self.density_name = "gdas"
            self.refr_name = "gdas"

    def density_at_height(self,h):
        v1 = self.vertical_depth_at_height(h+0.1)
        v2 = self.vertical_depth_at_height(h-0.1)
        return ((v2-v1)/0.2)/100

    def vertical_depth_at_height(self,h):
        if len(self.vdepth) == 0:
            print("caching vertical depth for model: ",self.density_name)
            if self.density_name == "USStandard":
                a = [-186.5562,-94.9199,0.61289,0,1.0469564e-10,2.614186472e-10];#g / cm^2
                b = [1222.6562,1144.9069,1305.5948,540.1778,540.17833,1.];#g / cm^2
                c = [9941.8638,8781.5355,6361.4304,7721.7016,7721.70097,1.64444076e15];#m
                lim  = [4.0e3,10.0e3,40.0e3,100.0e3,250e3,429.89e3];#m
                index = -1
                h_ = np.arange(0,int(np.max(lim)),1)
                for h__ in h_:
                    if h__ < lim[0]:
                        index = 0
                    for i in range(0,5):
                        if lim[i] < h__ <= lim[i+1]:
                            index = i+1
                            break
                    if 0 <= index < 5:
                        self.vdepth.append(a[index] + (b[index]*np.exp(-h__/c[index])))
                    elif index == 5:
                        self.vdepth.append(a[index] - (b[index]*h__/c[index]))
            elif self.density_name == "gdas":
                h_ = np.arange(0,int(np.max(self.gdas_atmlay)),1)
                for h__ in h_:
                    index = -1
                    for i in range(0,len(self.gdas_atmlay)-1):
                        if self.gdas_atmlay[i] < h__ <= self.gdas_atmlay[i+1]:
                            index = i
                            break
                    if index != -1:
                        self.vdepth.append(self.gdas_A[index] + (self.gdas_B[index]*np.exp(-h__/self.gdas_C[index])))
            print('..cached model',len(self.vdepth))
        ind = int(h)
        if (ind+1) >= len(self.vdepth):
            return 0
        else:
            X0 = self.vdepth[ind]
            X1 = self.vdepth[ind+1]
            return X0 + (h-ind)*(X1-X0)
    
    # @lru_cache
    def vertical_height(self,rho,z,z_gr):
        z_c = z + z_gr
        C = -(self.rEarth + z_c)**2 - rho**2 + self.rEarthSquared
        #B = self.dEarth
        return (-self.dEarth + np.sqrt(self.dEarthSquared - (4 * C))) / 2

    def slant_depth_lin(self,zenith,r,z_gr,useCurved=True,N=2e5,dr=5,clip_height=100e3):
        Nmax = ((clip_height/np.cos(zenith)) - r)/dr
        if N > Nmax:
            N = Nmax
        else:
            print("WARNING not enough steps for full integral ")
        if N < 0:
            return 0

        r_ = np.array([r + i*dr for i in range(0,int(N))])
        rho = np.sin(zenith)*r_
        z = np.cos(zenith)*r_
        if useCurved:
            z_v = self.vertical_height(rho,z,z_gr)
        else:
            z_v = z + z_gr
        if z_v[-1] > clip_height:
            N = np.argmax(z_v>clip_height)-1
        return np.sum([self.density_at_height(z_)*dr for z_ in z_v[0:int(N)]])*100 #g/cm2

    # @lru_cache
    def slant_depth(self,zenith,r,z_gr,useCurved=True,N=500):
        #log binning
        ll = np.linspace(-1,5.5,N+1)
        l=10**ll
        dr = l[1:]-l[0:-1]
        rc = l[0:-1] + dr/2
        r_ = r + rc
        rho = np.sin(zenith)*r_
        z = np.cos(zenith)*r_
        if useCurved:
            z_v = self.vertical_height(rho,z,z_gr)
        else:
            z_v = z + z_gr        
        return np.sum([self.density_at_height(z_)* dr_ for z_,dr_ in zip(z_v,dr)])*100 #g/cm2

    def distance_to_slant_depth(self,zenith,X,z_gr,reso=1,nconv=50):
        _X = 1e30
        delta = 1e6
        _r = 0
        cnt = 0
        if X > self.slant_depth(zenith,0,z_gr):
            print("WARNING: requested slant depth ( %.2f g / cm^2) is larger than slant depth at ground level ( %.2f g / cm^2)"%(X,self.slant_depth(zenith,0,z_gr)))
            return -1
        while np.abs(_X - X) > reso:
            delta /= 2
            if (_X > X):
                _r = _r + delta
            else:
                _r = _r - delta
            _X = self.slant_depth(zenith, _r, z_gr)
            cnt = cnt + 1            
            if cnt > nconv:
                print ("WARNING no convergence in %d steps, _X - X = %.2f, X = %.2f,X_ = %.2f, _r = %.2f"%(nconv,_X - X,X,_X,_r))
                return -1
        return _r

    def refractivity_at_height(self,h):
        if len(self.refrac) == 0:
            print('Caching refractive index model',self.refr_name)
            h_ = np.arange(0,500e3,1)
            if self.refr_name == "zhaires":
                n0 = 325e-6
                kr = -0.1218e-3
                self.refrac = n0 * np.exp(kr*h_)
            elif self.refr_name == "southpole":
                n0 = 327.4e-6
                kr =-0.1369e-3
                self.refrac = n0 * np.exp(kr*h_)
            elif self.refr_name == "gdas":
                i0 = np.argmax(self.gdas_h>0)
                self.refrac = self.gdas_n[i0:]-1
            print('done caching')
        if type(h) == int or type(h) == float or type(h) == np.float64:
            h=np.array([h])
        ind = np.array(h, dtype=int)
        zero_index = (ind+1 >= len(self.refrac))
        ind[zero_index] = 0
        n0 = self.refrac[ind]
        n1 = self.refrac[ind+1]
        n = n0 + (h-ind)*(n1-n0)
        n[zero_index] = 0
        return n


    def light_travel_time(self,p1,p2,zgr=0,n_step=50):
        #p2 = antenna
        #p1 = reference point
        p1 = np.array(p1)
        p2 = np.array(p2).reshape(-1, 3) #start at antenna
        axis = p1-p2
        dist = np.linalg.norm(axis, axis=1)
        axis = np.divide(axis, dist.reshape(-1,1))
        dl = np.linspace(0, dist, n_step+1)[1:]
        trackx = np.transpose(axis)[0]*dl+p2[:,0]
        tracky = np.transpose(axis)[1]*dl+p2[:,1]
        trackz = np.transpose(axis)[2]*dl+p2[:,2]
        track = np.array([trackx, tracky, trackz])
        rho = np.linalg.norm(track[:2], axis=0)
        z = self.vertical_height(rho, track[2], 0)
        av_nr = 1+np.mean(self.refractivity_at_height(z), axis=0)
        t = np.divide(np.multiply(dist, av_nr), self.speed_of_light)
        return t,dist
